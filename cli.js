#! /usr/bin/env node

const chalk = require('chalk')

const {
  defaultKey,
  defaultCert,
  defaultCommonName,
  defaultSubjectAltName,
  keygen
} = require('.')

const argv = require('minimist')(process.argv.slice(2))

if (argv.help || argv.h) {
  console.log('Generate a self-signed TLS certificate and add it to the trusted certificate store.')
  console.log()
  console.log('Command:')
  console.log(`${chalk.bold.green('tls-keygen')} ${chalk.bold('[key] [certificate] [--skip-entrust]')}`)
  console.log()
  console.log(`${chalk.bold('key')}`)
  console.log('File path to save the private key.')
  console.log(`${chalk.dim('Default: ./key.pem')}`)
  console.log()
  console.log(`${chalk.bold('certificate')}`)
  console.log('File path to save the public certificate.')
  console.log(`${chalk.dim('Default: ./cert.pem')}`)
  console.log()
  console.log(`${chalk.bold('--skip-entrust')}`)
  console.log('Skip registering the certificate with the trusted certificate store.')
  console.log()
  console.log(`${chalk.bold('--add-san <name>')}`)
  console.log('Adds Subject Alternative Name to the list. Use multiple times to add more than one.')
  console.log()
  console.log('Examples:')
  console.log(`${chalk.bold('tls-keygen')}`)
  console.log(`${chalk.bold('tls-keygen "key.pem" "cert.pem"')}`)
  console.log(`${chalk.bold('tls-keygen --skip-entrust')}`)
  console.log(`${chalk.bold('tls-keygen --add-san DNS:localhost.example.com --add-san IP:172.16.1.2')}`)
  console.log()
  process.exit()
}

if (typeof argv['skip-entrust'] === 'string' && argv._.length < 2) {
  argv._.unshift(argv['skip-entrust'])
  argv['skip-entrust'] = true
}

const key = argv._[0] || defaultKey
const cert = argv._[1] || defaultCert
const entrust = !argv['skip-entrust']
let subjectAltName = defaultSubjectAltName.concat(argv['add-san'] || [])

console.log(chalk.bold('Key:'))
console.log(`🔑 ${key}`)
console.log()
console.log(chalk.bold('Certificate:'))
console.log(`📜 ${cert}`)
console.log()
console.log(chalk.bold('Common Name:'))
console.log(`  - 🏷  ${defaultCommonName}`)
console.log()
console.log(chalk.bold('Subject Alternative Names:'))
for (const name of subjectAltName) {
  console.log(`  - 🏷  ${name}`)
}
console.log()

keygen({ key, cert, entrust, subjectAltName })
  .then(({ key, cert }) => {
    console.log('🔐 Done!')
  })
  .catch((error) => {
    console.log(error.toString())
    process.exit(1)
  })
